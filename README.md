# Contact Form 7 - MyLeadConverter Addon

The Contact Form 7 - MyLeadConverter Addon plugin sends new leads to MyLeadConverter via Contact Form 7 forms.

## Description
**This plugin requires Contact Form 7, version 3.2**

- - -

### Requirements

#### WordPress
This plugin was built and tested on WordPress version 3.4.1.

#### Contact Form 7
Contact Form 7 allows you to configure your forms. View the Contact Form 7 [docs](http://contactform7.com/docs/) for more information.

#### MyLeadConverter Account
A MyLeadConverter account will allow you to track and manage sales leads from both online and offline lead sources. MyLeadConverter can track and record phone calls and let you know where those
phone calls are coming from.

## Installation

1. Upload `cf7-myleadconverter-addon` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Obtain Account ID, API Token and your fieldset IDs from MyLeadConverter
1. Configure the MyLeadConverter section on the Contact Form 7 form settings page
