<?php
/*
Plugin Name: Contact Form 7 - MyLeadConverter Addon
Plugin URI: http://code.myleadconverter.com
Description: Create new leads in MyLeadConverter via Contact Form 7.
Author: MyLeadConverter
Author URI: http://www.myleadconverter.com
Version: 1.0.1
*/

require_once('mlc-php/mlc.php');

define('MYLEADCONVERTER_CF7_VERSION', '1.0.1');

if (!defined('MYLEADCONVERTER_CF7_BASENAME')) {
    define('MYLEADCONVERTER_CF7_BASENAME', plugin_basename( __FILE__ ));
}

new MyLeadConverter_CF7();



final class MyLeadConverter_CF7 {

    /** @var MyLeadConverter_CF7_UI */
    private $ui = null;


    public function __construct() {
        $this->ui = new MyLeadConverter_CF7_UI($this, 'wpcf7-mlc');

        // add actions
        add_action('wpcf7_after_save', array($this, 'save')); // save MLC form options
        add_action('wpcf7_before_send_mail', array($this, 'send')); // send the form to MLC
        add_filter('plugin_action_links_' . MYLEADCONVERTER_CF7_BASENAME, array($this, 'getActionLinks'));

        add_action('wp_footer', array($this, 'appendScript')); // append mlc.js to footer of each page
    }

    /**
     * @param WPCF7_ContactForm $cf The current CF7 form object.
     */
    public function save( $cf ) {
        update_option($this->ui->id . '_' . $cf->id, $_POST[$this->ui->id]);
    }

    /**
     * Send the form to MyLeadConverter.
     * @param WPCF7_ContactForm $cf The current CF7 form object.
     */
    public function send( $cf ) {
        // MLC form options
        $cf7_mlc = get_option($this->ui->id . '_' . $cf->id);
        $submission = WPCF7_Submission::get_instance();
        $data = $submission->get_posted_data();

        if ($cf7_mlc) {
            if ($cf7_mlc['enable-myleadconverter'] === 'true') {
                // remove cf7 fields
                unset($data['_wpcf7']);
                unset($data['_wpcf7_version']);
                unset($data['_wpcf7_unit_tag']);
                unset($data['_wpnonce']);
                unset($data['_wpcf7_is_ajax_call']);
                unset($data['_wpcf7_locale']);
                // remove captcha fields
                foreach ($this->array_fuzzy_search($data, 'captcha') as $k) unset($data[$k]);

                // combine array values
                foreach ($data as $key => $value) {
                    if (is_array($value)) $data[$key] = implode(', ', $value);
                }

                MyLeadConverter::connect($cf7_mlc['account-id'], $cf7_mlc['api-token'])->newLead($cf7_mlc['fieldset'], $data);
            }
        }
    }

    /**
     * @param array $links The default action links.
     * @return array The action links.
     */
    public function getActionLinks( $links ) {
        $wpcf7_link = esc_attr(admin_url('admin.php?page=wpcf7'));
        $settings_link = "<a href=\"$wpcf7_link\" title=\"MyLeadConverter settings are available after selecting a form.\">"
            . esc_html(__( 'Settings', 'wpcf7' ))
            . '</a>';

        array_unshift($links, $settings_link);
        return $links;
    }

    /**
     * Append the MyLeadConverter javascript code to the footer of each page.
     */
    public function appendScript() {
        $ver = MYLEADCONVERTER_CF7_VERSION;
        echo "\n<!-- Contact Form 7 - MyLeadConverter Addon $ver -->\n"
            . '<script type="text/javascript">(function(d, w, t) {'
        	. 'var m = d.createElement(t), s = d.getElementsByTagName(t)[0];'
        	. 'm.src = "//www.myleadconverter.com/mlc.js?d="+w.location.href;'
        	. 's.parentNode.insertBefore(m,s);'
        	. '}(document, window, "script"));</script>'
            . "\n\n";
    }

    public static function getPluginUrl( $path = '' ) {
        return plugins_url($path, MYLEADCONVERTER_CF7_BASENAME);
    }

    private function array_fuzzy_search( $arr, $in ) {
        $tmpkeys = array();
        $keys = array_keys($arr);
        foreach ($keys as $k) {
            if (stripos($k, $in) !== FALSE) $tmpkeys[] = $k;
        }
        return $tmpkeys;
    }

}

final class MyLeadConverter_CF7_UI {

    /** @var MyLeadConverter_CF7 */
    private $mlc = null;
    /** @var string */
    public $id = '';
    /** @var FieldsMetaBox */
    public $box = null;


    public function __construct( MyLeadConverter_CF7 $mlc, $id ) {
        $this->mlc = $mlc;
        $this->id = $id;

        // add actions
        add_action('wpcf7_admin_notices', array($this, 'formOptions')); // register form options ui meta box
        add_action('wpcf7_admin_after_form', array($this, 'append')); // append form options ui after form meta box
    }

    public function formOptions() {
        if (wpcf7_admin_has_edit_cap()) { // if current user can edit CF7 forms
            $this->box = new FieldsMetaBox($this->id, 'cf7mlcdiv', __('MyLeadConverter', 'wpcf7'), 'cfseven', 'cf7_mlc', 'core');
            $this->box->addField(new MetaBoxField('checkbox', 'Send To MyLeadConverter', 'enable-myleadconverter'));
            $this->box->addField(new MetaBoxField('text', 'Account ID', 'account-id'));
            $this->box->addField(new MetaBoxField('text', 'API Token', 'api-token'));
            $this->box->addField(new MetaBoxField('text', 'Fieldset ID', 'fieldset'));
        }
    }

    public function append( $cf ) {
        do_meta_boxes('cfseven', 'cf7_mlc', $cf);
    }

}


final class FieldsMetaBox {

    public $id = '';
    public $name = '';
    public $title = '';
    public $page = '';
    public $context = '';
    public $priority = '';
    public $fields = array();

    public function __construct( $id, $name, $title, $page, $context, $priority ) {
        $this->id = $id;
        $this->name = $name;
        $this->title = $title;
        $this->page = $page;
        $this->context = $context;
        $this->priority = $priority;

        add_meta_box($name, $title, array($this, 'build'), $page, $context, $priority);
    }

    public static function getInstance( $id, $name, $title, $page, $context, $priority ) {
        return new FieldsMetaBox($id, $name, $title, $page, $context, $priority);
    }

    public function addField( MetaBoxField $field ) {
        $this->fields[] = $field->box($this);
    }

    public function build( $args ) {
        $options = get_option($this->id . '_' . $args->id);
        $html= '<table class="form-table">';
        foreach ($this->fields as $field) {
            $meta = $options[$field->basename];
            $html .= "<tr>"
                . "<th style=\"width:20%;text-align:right;\"><label for=\"{$field->name}\">{$field->label}:</label></th>"
                . "<td>{$field->html($meta)}</td>"
                . "</tr>";
        }
        if (count($this->fields)==0) $html .= "<tr><td>There are no fields to display.</td></tr>";
        $html .= '</table>';
        echo $html;
    }

}


final class MetaBoxField {

    public $label = '';
    public $description = ''; // caption for field
    public $basename = ''; // name without prefix from parent box
    public $name = '';
    public $type = '';
    public $default = '';
    public $options = array();
    public $box = null;

    public function __construct( $type, $label, $name, $description = '', $default = '' ) {
        $this->label = $label;
        $this->description = $description;
        $this->basename = $name;
        $this->type = $type;
        $this->default = $default;
    }

    public function addOption( $name, $value = '' ) {
        if ($value = '') $value = $name;
        $this->options[] = array('name' => $name, 'value' => $value);
    }

    public function box( $box ) {
        $this->box = $box;
        $this->name = "{$box->id}[{$this->basename}]";
        return $this;
    }

    // addOptions(array('name' => '', 'value' => ''), array('name' => '', 'value' => ''))
    // addOptions(array( array('name' => '', 'value' => ''), array('name' => '', 'value' => '') ))
    public function addOptions() {
        $opts = func_get_args();
        if (func_num_args() == 1) {
            $o = func_get_arg(0);
            if (is_array($o)) $opts = $o;
        }
        $this->options = $this->options + $opts;
    }

    private function buildOption( $meta, $option ) {
        switch ($this->type) {
            case MetaBoxFieldType::SELECT:
                return "<option value=\"{$option['value']}\">{$option['name']}</option>";
                break;
            case MetaBoxFieldType::RADIO:
            case MetaBoxFieldType::CHECKBOX:
                $checked = ($meta == $option['value']) ? 'checked="checked"' : '';
                return "<label><input type=\"{$this->type}\" name=\"{$this->name}\" value=\"{$option['value']}\" {$checked} /> {$option['name']}</label>";
                break;
        }
        return '';
    }

    public function html( $meta ) {
        switch ($this->type) {
            case MetaBoxFieldType::TEXT:        return $this->text($meta);      break;
            case MetaBoxFieldType::TEXTAREA:    return $this->textarea($meta);  break;
            case MetaBoxFieldType::SELECT:      return $this->select($meta);    break;
            case MetaBoxFieldType::RADIO:       return $this->radio($meta);     break;
            case MetaBoxFieldType::CHECKBOX:    return $this->checkbox($meta);  break;
        }
        return '';
    }

    private function text( $meta ) {
        $value = $meta ? $meta : $this->default;
        return "<input type=\"text\" name=\"{$this->name}\" id=\"{$this->name}\" value=\"{$value}\" size=\"30\" style=\"width:97%\" />";
    }

    private function textarea( $meta ) {
        $value = $meta ? $meta : $this->default;
        return "<textarea name=\"{$this->name}\" id=\"{$this->name}\" cols=\"60\" rows=\"4\" style=\"width:97%\">{$value}</textarea>";
    }

    private function select( $meta ) {
        $opts = '';
        foreach ($this->options as $option) {
            $opts .= $this->buildOption($meta, $option);
        }
        return "<select name=\"{$this->name}\" id=\"{$this->name}\">{$opts}</select>";
    }

    private function radio( $meta ) {
        $html = '';
        if (empty($this->options)) {
            $value = $meta ? $meta : 'true';
            $checked = ($meta == $value) ? 'checked="checked"' : '';
            $html = "<input type=\"radio\" name=\"{$this->name}\" id=\"{$this->name}\" value=\"{$value}\" {$checked} />";
        } else {
            foreach ($this->options as $i => $option) {
                if ($i > 0) $html .= '<br/>';
                $html .= $this->buildOption($meta, $option);
            }
        }
        return $html;
    }

    private function checkbox( $meta ) {
        $html = '';
        if (empty($this->options)) {
            $value = $meta ? $meta : 'true';
            $checked = ($meta == $value) ? 'checked="checked"' : '';
            $html = "<input type=\"checkbox\" name=\"{$this->name}\" id=\"{$this->name}\" value=\"{$value}\" {$checked} />";
        } else {
            foreach ($this->options as $i => $option) {
                if ($i > 0) $html .= '<br/>';
                $html .= $this->buildOption($meta, $option);
            }
        }
        return $html;
    }

    public static function nonce( $name ) {
        return "<input type=\"hidden\" name=\"$name\" value=\"" . wp_create_nonce(MYLEADCONVERTER_CF7_BASENAME) . '" />';
    }

}

final class MetaBoxFieldType {

    const TEXT =        'text';
    const TEXTAREA =    'textarea';
    const SELECT =      'select';
    const RADIO =       'radio';
    const CHECKBOX =    'checkbox';
    const NONCE =       'nonce';

    public function __construct() { throw new Exception('This class cannot be instantiated.'); }

}
